function installFeaturesParallax() {	
	var f = function() {
		var scrollY = document.documentElement.scrollTop;
		var featuresDiv = document.querySelector(".div-features")
		var featuresY = featuresDiv.offsetTop;
		var featuresH = featuresDiv.offsetHeight;
		var featuresW = featuresDiv.offsetWidth;
		if (
				(scrollY > (featuresY - 300)) && 
				(scrollY < (featuresY + featuresH))
			) {
			var bgOffset = Math.round((scrollY - (featuresY - 300)) / featuresH * 300);			
			featuresDiv.style.backgroundPosition = "0px -" + bgOffset + "px";			
		}
		featuresDiv.style.backgroundSize = "" + featuresW + "px " + (featuresH + 400) + "px";
	};
	window.onscroll = f;
	window.onresize = f;
	f();
}

function installCollaborationSliderInterval() {
	var idList = ["#slide1", "#slide2", "#slide3"]
	var current = null;
	setInterval(function(){		
		if (current != null)
		{
			document.querySelector(current).checked = false	
		}
		var current = idList.pop();		
		idList.unshift(current);
		document.querySelector(current).checked = true
	}, 5000);
}

function init() {
	installFeaturesParallax();
	installCollaborationSliderInterval();
}