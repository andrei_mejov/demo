var gulp = require("gulp");
var jade = require("gulp-jade");
var less = require("gulp-less");
var del = require("del");

gulp.task("default", function() {
	del.sync("./dist/**");
	gulp.src("./pages/**.jade")
		.pipe(jade({pretty: true}))
		.pipe(gulp.dest("./dist/"));
	gulp.src("./images/**")
		.pipe(gulp.dest("./dist/images"));
	gulp.src("./js/**")
		.pipe(gulp.dest("./dist/js"));
	gulp.src("./less/main.less")
		.pipe(less())
		.pipe(gulp.dest("./dist/css"));
});