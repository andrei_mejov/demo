package test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.BorderLayout;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.jms.*;
import javax.swing.*;
import org.apache.activemq.*;
import org.apache.activemq.broker.*;
import org.apache.activemq.store.kahadb.*;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.*;
import org.eclipse.jetty.server.nio.*;
import com.google.gson.Gson;

public class Main
{
	public static class WwwService
	{
		private Server server;

		public WwwService(String host, Integer port)
		{
			server = new Server();
			SelectChannelConnector connector = new SelectChannelConnector();
			connector.setHost(host);
			connector.setPort(port);
			server.addConnector(connector);
	 
			ResourceHandler resource_handler = new ResourceHandler();
			resource_handler.setDirectoriesListed(false);
			resource_handler.setWelcomeFiles(new String[]{"index.html"});
	 
			resource_handler.setResourceBase("./www");
	 
			HandlerList handlers = new HandlerList();
			handlers.setHandlers(new Handler[] {
				resource_handler, new DefaultHandler()
			});
			server.setHandler(handlers);	 	        
		}

		public void start() throws Throwable
		{
			server.start();
		}

		public void stop() throws Throwable
		{
			server.stop();
		}
	}

	public static class MessageService
	{
		private static MessageService instance;
		private static Gson gson = new Gson();

		public static MessageService getInstance() {return instance;}

		private BrokerService brokerService;
		private String host;
		private Integer port;

		public BrokerService getBrokerService() {return brokerService;}
		public String getHost() {return host;}
		public Integer getPort() {return port;}

		public MessageService() throws Throwable		
		{
			this("0.0.0.0", 61616);			
		}

		public MessageService(String host, Integer port) throws Throwable
		{
			instance = this;
			this.host = host;
			this.port = port;

			brokerService = new BrokerService();
			brokerService.setUseShutdownHook(true);

			TransportConnector connector = new TransportConnector();
			connector.setUri(new URI("tcp://" + host + ":" + port));
			brokerService.addConnector(connector);

			TransportConnector wsConnector = new TransportConnector();
			wsConnector.setUri(new URI("ws://" + host + ":" + (port + 1)));
			brokerService.addConnector(wsConnector);

			KahaDBPersistenceAdapter persistenceAdapter = new KahaDBPersistenceAdapter();
			persistenceAdapter.setDirectory(new File("data"));
			brokerService.setPersistenceAdapter(persistenceAdapter);			
		}

		public void start() throws Throwable
		{
			brokerService.start();
		}

		public void stop() throws Throwable
		{					
			brokerService.stop();
		}

		public static String toJson(Object value)
		{
			return gson.toJson(value);
		}

		public static <T> T fromJson(String jsonString, Class<T> clazz)
		{
			return gson.fromJson(jsonString, clazz);
		}

		public static <T> T extractFromMessage(javax.jms.Message message, Class<T> clazz) 
			throws Throwable
		{
			if (message instanceof ObjectMessage)
			{
				Object o = ((ObjectMessage)message).getObject();
				if (clazz.isInstance(o))
				{
					return clazz.cast(o);
				}
				else
				{
					return null;
				}
			}
			else if (message instanceof TextMessage)
			{
				return MessageService.fromJson(
					((TextMessage)message).getText(), clazz
				);
			}
			else if (message instanceof BytesMessage)
			{
				BytesMessage bm = (BytesMessage)message;
				byte[] buf = new byte[(int)bm.getBodyLength()];
				bm.readBytes(buf);
				String s = new String(buf, "UTF-8");
				return MessageService.fromJson(s, clazz);
			}
			else
			{
				return null;
			}
		}

		public static void processSession(String url, SessionProcessor sp, ExceptionProcessor ep) 			
		{
			try
			{
				ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
				javax.jms.Connection connection = connectionFactory.createConnection(); 
				connection.start();            
				try
				{
					javax.jms.Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
					try
					{
						sp.process(session);
					}
					finally
					{
						session.close();
					}
				}            
				finally
				{
					connection.close();
				}
			}
			catch (Throwable t)
			{
				try
				{
					ep.process(t);
				}
				catch (Throwable x) {}
			}
		}

		public static void processQueue(
			javax.jms.Session session, String queueName, QueueProcessor qp, ExceptionProcessor ep
		)
		{
			Destination queue = null;
			try
			{				
				queue = session.createQueue(queueName);
				qp.process((javax.jms.Queue)queue);				
			}
			catch (Throwable t)
			{
				try
				{
					ep.process(t);
				}
				catch (Throwable x) {}
			}			
		}

		public static void asyncProcessMessageFrom(
			javax.jms.Session session, String queueName, MessageProcessor mp, ExceptionProcessor ep
		)
		{
			try
			{
				Destination queue = session.createQueue(queueName);
				MessageConsumer consumer = session.createConsumer(queue);
				consumer.setMessageListener(
					(javax.jms.Message m) -> {
						try
						{
							mp.process(m);
						}
						catch (Throwable t)
						{
							try
							{
								ep.process(t);
							}
							catch (Throwable x) {}
						}
					}
				);
			}
			catch (Throwable t)
			{
				try
				{
					ep.process(t);
				}
				catch (Throwable x) {}
			}
		}
	}

	public static interface SessionProcessor
	{
		public void process(javax.jms.Session session) throws Throwable;
	}

	public static interface QueueProcessor
	{
		public void process(javax.jms.Queue queue) throws Throwable;
	}

	public static interface MessageProcessor
	{
		public void process(javax.jms.Message message) throws Throwable;
	}

	public static interface ExceptionProcessor
	{
		public void process(Throwable t) throws Throwable;
	}

	public static class BackgroundOperation extends Thread
	{
		private Runnable innerRunnable;
		private ExceptionProcessor exceptionProcessor;

		public BackgroundOperation(Runnable runnable, ExceptionProcessor ep)
		{
			this.innerRunnable = runnable;
			this.exceptionProcessor = ep;
		}

		public void run()
		{
			try
			{
				innerRunnable.run();
			}
			catch (Throwable t)
			{
				try
				{
					exceptionProcessor.process(t);
				}
				catch (Throwable x) {}
			}
		}		
	}

	public static void defaultExceptionProcessor(Throwable t)
	{
		t.printStackTrace();
	}

	public static class Pixel implements Serializable
	{
		private Double x;
		private Double y;
		private Integer  xs;
		private Integer  ys;

		private Integer red;
		private Integer green;
		private Integer blue;		

		public Double getX() {return x;}
		public Double getY() {return y;}
		public Integer getXs() {return xs;}
		public Integer getYs() {return ys;}

		public Integer getRed() {return red;}
		public void setRed(Integer red) {this.red = red;}

		public Integer getGreen() {return green;}
		public void setGreen(Integer green) {this.green = green;}

		public Integer getBlue() {return blue;}
		public void setBlue(Integer blue) {this.blue = blue;}

		public Pixel(Double x, Double y, Integer xs, Integer ys)
		{			
			this.x = x; this.y = y;
			this.xs = xs; this.ys = ys;
			this.red = 0; this.green = 0; this.blue = 0;
		}
	}

	public static class PixelList implements Serializable
	{
		private List<Pixel> list = new ArrayList<Pixel>();

		public List<Pixel> getList() {return list;}
	}

	public static class MainWindow extends JFrame implements MessageProcessor
	{		
		private static JPanel panel;
		private static BufferedImage image;

		public MainWindow()
		{			
			super("Ex7: JMS Fractal");
			setSize(640, 480);

			setLayout(new BorderLayout());

			panel = new JPanel() {
				public void paint(Graphics g) {
					super.paint(g);
					g.drawImage(image, 0, 0, Color.WHITE, null);					
				}
			};		
			panel.setVisible(true);
			this.add(panel, BorderLayout.CENTER);
			image = new BufferedImage(
				this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB
			);			
		}		

		public void process(javax.jms.Message message) throws Throwable
		{			
			PixelList pixelList = MessageService.extractFromMessage(message, PixelList.class);
			if (pixelList != null)
			{
				Graphics g = image.getGraphics();
				for (Pixel p : pixelList.getList())
				{
					g.setColor(
						new Color(p.getRed(), p.getGreen(), p.getBlue())
					);
					g.fillRect((int)p.getXs(), (int)p.getYs(), 1, 1);
					System.out.println("Render pixel: " + p.getXs() + " " + p.getYs());
				}
				MainWindow _this = this;
				SwingUtilities.invokeLater(() -> {
					_this.repaint();
				});
			}
		}
	}

	public static void renderSession(javax.jms.Session session) throws Throwable
	{
		final MainWindow window = new MainWindow();
		window.setVisible(true);

		MessageService.asyncProcessMessageFrom(
			session, "render",
			(javax.jms.Message message) -> {				
				window.process(message);
				session.commit();
			},
			(Throwable t) -> {
				t.printStackTrace();
				session.rollback();
			}
		);

		MessageService.processQueue(
			session, "fractal",
			(javax.jms.Queue fractalQueue) -> {
				MessageProducer producer = session.createProducer(fractalQueue);
				PixelList pixelList = new PixelList();

				final Double yStep = 2.0 / window.getHeight();
				final Double xStep = 3.0 / window.getWidth();
				Integer ys = 0;				
				for (Double y = -1.0; y <= 1.0; y += yStep)
				{
					ys += 1;
					Integer xs = 0;
					for (Double x = -2.0; x <= 1.0; x += xStep)
					{
						xs += 1;
						Pixel p = new Pixel(
							x, y,
							xs, ys
						);
						pixelList.getList().add(p);
						if (pixelList.getList().size() > 100)
						{
							TextMessage tm = session.createTextMessage();
							tm.setText(MessageService.toJson(pixelList));
							producer.send(tm);
							session.commit();
							pixelList = new PixelList();
						}					
					}//for x					
				}//for y
				if (pixelList.getList().size() > 0)
				{
					TextMessage tm = session.createTextMessage();
					tm.setText(MessageService.toJson(pixelList));
					producer.send(tm);
					session.commit();
				}
			},
			(Throwable t) -> {
				t.printStackTrace();
				session.rollback();								
			}
		);		

		while (true)
		{								
			try
			{
				Thread.sleep(1000L);
			}
			catch (Throwable t) {}
		}
	}

	public static void fractalSession(javax.jms.Session session) throws Throwable
	{
		javax.jms.Queue renderQueue = session.createQueue("render");
		final MessageProducer producer = session.createProducer(renderQueue);

		MessageService.asyncProcessMessageFrom(
			session, "fractal",
			(javax.jms.Message message) -> {
				PixelList pixelList = MessageService.extractFromMessage(message, PixelList.class);
				if (pixelList != null)
				{
					for (Pixel p : pixelList.getList())
					{
						System.out.println("Process pixel: " + p.getXs() + " " + p.getYs());

						Double curX = p.getX();
						Double curY = p.getY();
						Double ix = 0.0;
						Double iy = 0.0;
						Integer n = 0;

						while (((ix * ix) + (iy * iy)) < 5 && n < 64) 
						{
							ix = curX * curX  - curY * curY + p.getX();
							iy = 2.0 * curX * curY + p.getY();
							curX = ix;
							curY = iy;
							n++;
						}

						Integer red = 0;
						Integer green = 0;
						Integer blue = 0;
						if (n < 7) {red = n * 20; green =0 ; blue = 0;}
						else if (n < 20) {red =  n * 10; green = n * 5; blue = 0;}
						else if (n < 32) {red = 0; green = n * 3; blue = n * 5;}
						else if (n < 50) {red = 0; green =0; blue = n * 5;}
						else if (n < 64) {red = n * 5; green = 0; blue = n * 5;}
						p.setRed(Math.min(red, 255));
						p.setGreen(Math.min(green, 255));
						p.setBlue(Math.min(blue, 255));						
					}
					TextMessage tm = session.createTextMessage();
					tm.setText(MessageService.toJson(pixelList));
					producer.send(tm);					
				}
				session.commit();			
			},
			(Throwable t) -> {
				t.printStackTrace();
				session.rollback();
			}
		);

		while (true)
		{								
			try
			{
				Thread.sleep(1000L);
			}
			catch (Throwable t) {}
		}
	}

	public static void main(String[] args)
	{
		String host = "0.0.0.0";
		Integer port = 61616;		
		try
		{	
			if (args.length == 0) 
			{
				System.out.println("run.sh <bus|render|fractal>");
			}
			else if (args[0].equals("bus"))
			{
				System.setProperty(
					"org.eclipse.jetty.util.log.class",
					"org.eclipse.jetty.util.log.StdErrLog"
				);
				MessageService service = new MessageService(host, port);
				WwwService wwwService = new WwwService(host, 8080);
				service.start();
				wwwService.start();
				System.in.read();
				service.stop();
				wwwService.stop();		
			}
			else if (args[0].equals("render"))
			{		
				MessageService.processSession(
					"tcp://" + host + ":" + port,
					Main::renderSession,
					Main::defaultExceptionProcessor
				);
			}
			else
			{
				MessageService.processSession(
					"tcp://" + host + ":" + port,
					Main::fractalSession,
					Main::defaultExceptionProcessor	
				);
			}
			
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}
	}
}