package test;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.BorderLayout;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import javax.jms.*;
import javax.swing.*;
import org.apache.activemq.*;
import org.apache.activemq.broker.*;
import org.apache.activemq.broker.region.policy.*;
import org.apache.activemq.store.kahadb.*;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.*;
import org.eclipse.jetty.server.nio.*;
import com.google.gson.Gson;

public class Main
{
	public static interface ExceptionProcessor
	{
		public void process(Throwable t) throws Throwable;
	}

	@Retention(RetentionPolicy.RUNTIME)
	public static @interface JMSService
	{		
		Class impl();
	}

	@Retention(RetentionPolicy.RUNTIME)
	public static @interface JMSInject
	{
	}

	@Retention(RetentionPolicy.RUNTIME)
	public static @interface JMSMethod
	{
		boolean oneWay() default false;
		String queueName();		
	}

	public static class JMSInvoke implements Serializable
	{
		private String methodName;
		private String queueName;
		private Object[] parameters;

		public String getMethodName() {return methodName;}
		public void setMethodName(String methodName) {this.methodName = methodName;}

		public String getQueueName() {return queueName;}
		public void setQueueName(String queueName) {this.queueName = queueName;}

		public Object[] getParameters() {return parameters;}
		public void setParameters(Object[] parameters) {this.parameters = parameters;}

		public JMSInvoke() {}
	}

	public static class JMSInvokeResult implements Serializable
	{
		private Object result;
		private Throwable exception;

		public Object getResult() {return result;}
		public Throwable getException() {return exception;}

		public JMSInvokeResult(Object result, Throwable exception)
		{
			this.result = result;
			this.exception = exception;
		}
	}

	public static interface JMSAsyncHandler
	{
		public void process(Object result, Throwable exception);
	}

	public static class JMSServiceContainer
	{
		private javax.jms.Session session;
		private javax.jms.Queue resultQueue;
		private Object serviceImpl;
		private Class serviceInterface;
		private ConcurrentHashMap<String, String> queues = 
			new ConcurrentHashMap<String, String>();
		private ConcurrentHashMap<String, MessageListener> listeners = 
			new ConcurrentHashMap<String, MessageListener>();
		private ConcurrentHashMap<Destination, MessageProducer>	 producers =
			new ConcurrentHashMap<Destination, MessageProducer>();

		public javax.jms.Session getSession() {return session;}
		public Object getServiceImpl() {return serviceImpl;}
		public Class getServiceInterface() {return serviceInterface;}

		private MessageProducer getProducer(Destination queue) throws JMSException
		{
			if (!producers.containsKey(queue))
			{				
				MessageProducer producer = session.createProducer(queue);
				producers.put(queue, producer);
			}
			return producers.get(queue);
		}

		private void installListener(final Method method, final String queueName)
		{
			try
			{
				Destination queue = session.createQueue(queueName);
				MessageConsumer consumer = session.createConsumer(queue);
				consumer.setMessageListener(
					(javax.jms.Message m) -> {
						try
						{							
							Destination returnQueue = m.getJMSReplyTo();
							if (m instanceof ObjectMessage)
							{
								Object o = ((ObjectMessage)m).getObject();
								if (o instanceof JMSInvoke)
								{
									JMSInvoke ji = (JMSInvoke)o;
									if (!ji.getMethodName().equals(method.getName())) 
									{
										throw new RuntimeException(
											"Illegal route, wrong method: " + ji.getMethodName() + 
											"/" + method.getName()
										);
									}
									if (!ji.getQueueName().equals(queueName))
									{
										throw new RuntimeException(
											"Illegal route, wrong queue: " + ji.getQueueName() +
											"/" + queueName
										);
									}
									System.out.println(
										"Invoking: method " + method.getName() + " in queue " + queueName
									);
									Object result = null;
									Throwable exception = null;
									try
									{
										result = method.invoke(serviceImpl, ji.getParameters());
									}
									catch (Throwable t)
									{
										exception = t;
									}
									JMSInvokeResult jir = new JMSInvokeResult(result, exception);

									MessageProducer producer = getProducer(returnQueue);
									ObjectMessage resultMessage = session.createObjectMessage();
									resultMessage.setJMSCorrelationID(m.getJMSCorrelationID());
									resultMessage.setObject(jir);
									producer.send(resultMessage);									
								}
							}							
						}
						catch (Throwable e)
						{
							throw new RuntimeException(e);
						}
					}
				);
			}
			catch (JMSException e)
			{
				throw new RuntimeException(e);
			}
		}		

		public JMSServiceContainer(JMS jms, javax.jms.Session session, Object serviceImpl, Class serviceInterface)
		{
			this.session = session;
			this.serviceImpl = serviceImpl;
			this.serviceInterface = serviceInterface;

			try
			{
				this.resultQueue = this.session.createTemporaryQueue();				
			}
			catch  (JMSException e)
			{
				throw new RuntimeException(e);
			}

			if (!serviceInterface.isAnnotationPresent(JMSService.class))
			{
				throw new RuntimeException("Annotation @JMSService not found on interface");
			}
			JMSService jmsServiceAnnotation = (JMSService)serviceInterface.getAnnotation(JMSService.class);
			if (!jmsServiceAnnotation.impl().isInstance(serviceImpl))
			{
				throw new RuntimeException("JMSService impl mismatch");
			}

			for (Method method : serviceInterface.getMethods())
			{
				if (method.isAnnotationPresent(JMSMethod.class))
				{
					JMSMethod jmsMethodAnnotation = (JMSMethod)method.getAnnotation(JMSMethod.class);
					if (queues.containsKey(method.getName()))
					{
						throw new RuntimeException("Method already exist: " + method.getName());
					}
					queues.put(method.getName(), jmsMethodAnnotation.queueName());					
					installListener(method, jmsMethodAnnotation.queueName());					
				}
			}
			try
			{
				for (Field field : serviceImpl.getClass().getDeclaredFields())
				{
					if (field.isAnnotationPresent(JMSInject.class))
					{
						field.setAccessible(true);
						Class si = field.getType();
						if (si.equals(JMS.class))
						{
							field.set(serviceImpl, jms);	
						}
						else
						{
							Object proxy = jms.createProxy(si);						
							field.set(serviceImpl, proxy);
						}
					}
				}
			}
			catch (Throwable t)
			{
				throw new RuntimeException(t);
			}
		}
	}

	public static class JMSClient implements InvocationHandler
	{
		private javax.jms.Session session;
		private javax.jms.Queue resultQueue;
		private Class serviceInterface;		

		private ConcurrentHashMap<String, Boolean> oneWays = 
			new ConcurrentHashMap<String, Boolean>();

		private ConcurrentHashMap<String, String> queues = 
			new ConcurrentHashMap<String, String>();

		private ConcurrentHashMap<Destination, MessageProducer> producers =
			new ConcurrentHashMap<Destination, MessageProducer>();

		public javax.jms.Session getSession() {return session;}
		public Class getServiceInterface() {return serviceInterface;}

		public JMSClient(javax.jms.Session session, Class serviceInterface)
		{
			this.session = session;
			this.serviceInterface = serviceInterface;

			try
			{
				this.resultQueue = this.session.createTemporaryQueue();				
			}
			catch  (JMSException e)
			{
				throw new RuntimeException(e);
			}

			if (!serviceInterface.isAnnotationPresent(JMSService.class))
			{
				throw new RuntimeException("Annotation @JMSService not found on interface");
			}
			JMSService jmsServiceAnnotation = (JMSService)serviceInterface.getAnnotation(JMSService.class);
			
			for (Method method : serviceInterface.getMethods())
			{
				if (method.isAnnotationPresent(JMSMethod.class))
				{
					JMSMethod jmsMethodAnnotation = (JMSMethod)method.getAnnotation(JMSMethod.class);
					if (queues.containsKey(method.getName()))
					{
						throw new RuntimeException("Method already exist: " + method.getName());
					}
					queues.put(method.getName(), jmsMethodAnnotation.queueName());
					if (jmsMethodAnnotation.oneWay())
					{
						oneWays.put(method.getName(), true);
					}
				}
			}
		}

		private MessageProducer getProducer(Destination queue) throws JMSException
		{
			if (!producers.containsKey(queue))
			{				
				MessageProducer producer = session.createProducer(queue);
				producers.put(queue, producer);
			}
			return producers.get(queue);
		}

		public Object invoke(Object proxy, Method method, Object[] parameters)
		{
			if (!queues.containsKey(method.getName()))
			{
				throw new RuntimeException("JMS method not found: " + method.getName());
			}
			try
			{
				String correlationId = serviceInterface.getName() + ":" + method.getName() + ":" +
					UUID.randomUUID().toString();
				String queueName = queues.get(method.getName());
				Destination queue = session.createQueue(queueName);
				MessageProducer producer = getProducer(queue);
				ObjectMessage om = session.createObjectMessage();
				JMSInvoke ji = new JMSInvoke();
				ji.setMethodName(method.getName());
				ji.setQueueName(queueName);
				ji.setParameters(parameters);
				om.setJMSReplyTo(resultQueue);
				om.setJMSCorrelationID(correlationId);
				om.setObject(ji);

				if (oneWays.containsKey(method.getName()))
				{
					producer.send(om);
					return null;
				}
				else
				{
					MessageConsumer resultConsumer = session.createConsumer(
						resultQueue,
						"JMSCorrelationID='" + correlationId + "'"
					);
					final Object sem = new Object();
					final Object[] ra =  new Object[1];
					resultConsumer.setMessageListener(
						(javax.jms.Message m) -> {
							try
							{
								JMSInvokeResult jir = (JMSInvokeResult)
									(((ObjectMessage)m).getObject());
								ra[0] = jir;	
								synchronized (sem)
								{
									sem.notifyAll();
								}
							}
							catch (JMSException e)
							{
								throw new RuntimeException(e);
							}
						}
					);

					producer.send(om);

					if (ra[0] == null) 
					{		
						synchronized (sem)
						{
							sem.wait();
						}
					}
					resultConsumer.close();				
					JMSInvokeResult jir = (JMSInvokeResult)ra[0];
					if (jir.getException() != null)
					{
						throw new RuntimeException(jir.getException());
					}
					else
					{
						return jir.getResult();
					}
				}
			}
			catch  (Throwable t)
			{
				throw new RuntimeException(t);
			}
		}

		public Object createProxy()
		{
			return  java.lang.reflect.Proxy.newProxyInstance(
				serviceInterface.getClassLoader(),
				new Class[] {serviceInterface},
				this
			);
		}
	}

	public static class JMS	
	{
		private ExecutorService asyncThreadPool = Executors.newFixedThreadPool(32);

		private BrokerService brokerService;
		private javax.jms.Connection connection;

		private String host;
		private Integer port;

		private ConcurrentHashMap<Class, JMSServiceContainer> containers = 
			new ConcurrentHashMap<Class, JMSServiceContainer>();
		private ConcurrentHashMap<Class, JMSClient> clients = 
			new ConcurrentHashMap<Class, JMSClient>();		

		public String getHost() {return this.host;}
		public Integer getPort() {return this.port;}

		private String buildURL()
		{
			return "tcp://" + host + ":" + port;
		}

		private String buildClientURL()
		{
			if (host.equals("0.0.0.0"))
			{
				return "tcp://127.0.0.1:" + port;
			}
			else
			{
				return "tcp://" + host + ":" + port;
			}
		}

		public JMS(String host, Integer port, Boolean startServer) throws Throwable		 
		{
			this.host = host;
			this.port = port;			

			if (startServer)
			{
				System.setProperty("org.apache.activemq.UseDedicatedTaskRunner", "false");
				brokerService = new BrokerService();
				brokerService.setUseShutdownHook(true);
				brokerService.setPersistent(true);
				
				PolicyMap pm = new PolicyMap();
				PolicyEntry qpe = new PolicyEntry();
				qpe.setQueue(">");
				qpe.setMemoryLimit(64 * 1000 * 1000L);
				qpe.setProducerFlowControl(false);
				qpe.setQueuePrefetch(10);
				qpe.setAdvisoryWhenFull(true);
				pm.setPolicyEntries(Arrays.asList(new PolicyEntry[]{qpe}));
				brokerService.setDestinationPolicy(pm);

				TransportConnector connector = new TransportConnector();
				connector.setUri(new URI(buildURL()));
				brokerService.addConnector(connector);

				KahaDBPersistenceAdapter persistenceAdapter = new KahaDBPersistenceAdapter();
				persistenceAdapter.setDirectory(new File("data"));
				brokerService.setPersistenceAdapter(persistenceAdapter);
			}			
		}

		synchronized public void registerService(Object serviceImpl, Class serviceInterface)
		{
			if (containers.containsKey(serviceInterface))
			{
				throw new RuntimeException("Service already registered: " + serviceInterface);
			}						
			try
			{
				javax.jms.Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				JMSServiceContainer container = new JMSServiceContainer(this,  session, serviceImpl, serviceInterface);
				containers.put(serviceInterface, container);
			}
			catch (JMSException e)
			{
				throw new RuntimeException(e);
			}
		}

		synchronized public Object createProxy(Class serviceInterface)
		{
			if (!clients.containsKey(serviceInterface))
			{				
				try
				{
					javax.jms.Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
					JMSClient client = new JMSClient(session, serviceInterface);
					clients.put(serviceInterface, client);
				}
				catch (JMSException e)
				{
					throw new RuntimeException(e);
				}
			}
			return clients.get(serviceInterface).createProxy();
		}

		public void invokeAsync(
			Class serviceInterface, String methodName, Object[] parameters, final JMSAsyncHandler asyncHandler
		)
		{
			final Object proxy = createProxy(serviceInterface);
			final JMSClient client = clients.get(serviceInterface);
			asyncThreadPool.execute(() -> {				
				try
				{
					Method method = null;
					for (Method m : serviceInterface.getDeclaredMethods())
					{
						if (m.getName().equals(methodName))
						{
							method = m;
						}
					}
					if (method == null)
					{
						throw new RuntimeException("Method not found: " + methodName);
					}
					Object result = client.invoke(proxy, method, parameters);
					asyncHandler.process(result, null);
				}
				catch (Throwable t)
				{
					asyncHandler.process(null, t);
				}
			});			
		}

		public void start() throws Throwable
		{
			if (brokerService != null)
			{
				brokerService.start();
			}
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(buildClientURL());
			connection = connectionFactory.createConnection();
			connection.start();
		}

		public void stop() throws Throwable
		{
			containers.keySet().forEach((Class si) -> {
				try
				{
					containers.get(si).getSession().close();
				}
				catch (Throwable t) {t.printStackTrace();}
			});
			clients.keySet().forEach((Class si) -> {
				try
				{
					clients.get(si).getSession().close();
				}
				catch (Throwable t) {t.printStackTrace();}
			});
			if (connection != null)
			{
				try
				{
					connection.stop();
				}
				catch (Throwable t) {t.printStackTrace();}
			}			
			if (brokerService != null)
			{
				try
				{
					brokerService.stop();
				}
				catch (Throwable t) {t.printStackTrace();}
			}
			System.out.println("JMS stopped");
		}
	}

	@JMSService(impl = LogServiceImpl.class)
	public static interface LogService
	{
		@JMSMethod(queueName = "log", oneWay = true)
		public void log(String message);
	}

	public static class LogServiceImpl implements LogService
	{
		public void log(String message)
		{
			System.out.println(">>" + message);
		}
	}

	public static class Pixel implements Serializable
	{
		private Double x;
		private Double y;
		private Integer  xs;
		private Integer  ys;

		private Integer red;
		private Integer green;
		private Integer blue;		

		public Double getX() {return x;}
		public Double getY() {return y;}
		public Integer getXs() {return xs;}
		public Integer getYs() {return ys;}

		public Integer getRed() {return red;}
		public void setRed(Integer red) {this.red = red;}

		public Integer getGreen() {return green;}
		public void setGreen(Integer green) {this.green = green;}

		public Integer getBlue() {return blue;}
		public void setBlue(Integer blue) {this.blue = blue;}

		public Pixel(Double x, Double y, Integer xs, Integer ys)
		{			
			this.x = x; this.y = y;
			this.xs = xs; this.ys = ys;
			this.red = 0; this.green = 0; this.blue = 0;
		}
	}

	public static class PixelList implements Serializable
	{
		private List<Pixel> list = new ArrayList<Pixel>();

		public List<Pixel> getList() {return list;}
	}

	@JMSService(impl = FractalService.class)
	public static interface FractalService
	{
		@JMSMethod(queueName = "fractal")
		public PixelList calculateColor(PixelList pixelList);
	}

	public static class FractalServiceImpl implements FractalService
	{
		@JMSInject
		private LogService ls;

		@JMSInject
		private JMS jms;

		public PixelList calculateColor(PixelList pixelList)
		{
			if (!pixelList.getList().isEmpty())
			{
				ls.log("Process pixel from: " + pixelList.getList().get(0).getXs() + " " +
					pixelList.getList().get(0).getYs()
				);
			}
			for (Pixel p : pixelList.getList())
			{
				System.out.println("Local process pixel: " + p.getXs() + " " + p.getYs());				
				/*jms.invokeAsync(
					LogService.class, "log", new Object[] {"Process pixel: " + p.getXs() + " " + p.getYs()},
					(Object result, Throwable exception) -> {}
				);*/				

				Double curX = p.getX();
				Double curY = p.getY();
				Double ix = 0.0;
				Double iy = 0.0;
				Integer n = 0;

				while (((ix * ix) + (iy * iy)) < 5 && n < 64) 
				{
					ix = curX * curX  - curY * curY + p.getX();
					iy = 2.0 * curX * curY + p.getY();
					curX = ix;
					curY = iy;
					n++;
				}

				Integer red = 0;
				Integer green = 0;
				Integer blue = 0;
				if (n < 7) {red = n * 20; green =0 ; blue = 0;}
				else if (n < 20) {red =  n * 10; green = n * 5; blue = 0;}
				else if (n < 32) {red = 0; green = n * 3; blue = n * 5;}
				else if (n < 50) {red = 0; green =0; blue = n * 5;}
				else if (n < 64) {red = n * 5; green = 0; blue = n * 5;}
				p.setRed(Math.min(red, 255));
				p.setGreen(Math.min(green, 255));
				p.setBlue(Math.min(blue, 255));						
			}
			return pixelList;
		}
	}

	public static class MainWindow extends JFrame
	{		
		private static JPanel panel;
		private static BufferedImage image;
		private static Boolean end = false;

		private JMS jms;
		private LogService ls;

		public void newImage()
		{
			image = new BufferedImage(
				this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB
			);
		}

		public MainWindow(JMS jms)
		{			
			super("Ex8: JMSService");
			this.jms = jms;
			ls = (LogService)jms.createProxy(LogService.class);
			setSize(640, 480);

			setLayout(new BorderLayout());

			panel = new JPanel() {
				public void paint(Graphics g) {
					super.paint(g);
					g.drawImage(image, 0, 0, Color.WHITE, null);					
				}
			};		
			panel.setVisible(true);
			this.add(panel, BorderLayout.CENTER);
			newImage();
		}

		private void drawPixelList(PixelList pixelList)
		{
			if (!pixelList.getList().isEmpty())
			{
				ls.log(
					"Render pixel from: " + pixelList.getList().get(0).getXs() + " " + 
					pixelList.getList().get(0).getYs()
				);
			}
			Graphics g = image.getGraphics();
			for (Pixel p : pixelList.getList())
			{
				if (p.getXs().equals(639) && 
					p.getYs().equals(479)) 
				{
					end = true;
				}
				g.setColor(
					new Color(p.getRed(), p.getGreen(), p.getBlue())
				);
				g.fillRect((int)p.getXs(), (int)p.getYs(), 1, 1);				
			}
			MainWindow _this = this;
			SwingUtilities.invokeLater(() -> {
				_this.repaint();
			});
		}

		public void start()
		{
			Thread t = new Thread(() -> {
				FractalService fs = (FractalService)jms.createProxy(FractalService.class);
				while (true)
				{						
					PixelList pixelList = new PixelList();			
					try
					{
						end = false;
						newImage();						
						final Double yStep = 2.0 / getHeight();
						final Double xStep = 3.0 / getWidth();
						Integer ys = 0;				
						for (Double y = -1.0; y <= 1.0; y += yStep)
						{
							ys += 1;
							Integer xs = 0;
							for (Double x = -2.0; x <= 1.0; x += xStep)
							{
								xs += 1;
								Pixel p = new Pixel(
									x, y,
									xs, ys
								);
								pixelList.getList().add(p);
								if (pixelList.getList().size() > 1000)
								{									
									jms.invokeAsync(
										FractalService.class, "calculateColor", new Object[] {pixelList},
										(Object result, Throwable exception) -> {											
											if (exception != null) 
											{
												throw new RuntimeException(exception);
											}
											else
											{												
												drawPixelList((PixelList)result);
											}
										}
									);
									pixelList = new PixelList();									
								}					
							}//for x					
						}//for y
						if (pixelList.getList().size() > 0)
						{
							jms.invokeAsync(
								FractalService.class, "calculateColor", new Object[] {pixelList},
								(Object result, Throwable exception) -> {
									if (exception != null) 
									{
										throw new RuntimeException(exception);
									}
									else
									{
										drawPixelList((PixelList)result);
									}
								}
							);
							pixelList = new PixelList();
						}
						while (!end)
						{
							Thread.sleep(1000L);
						}
					}
					catch (Throwable x) 
					{
						x.printStackTrace();
					}
				}//while
			});
			t.start();
		}		
	}

	public static void main(String[] args)
	{
		String host = "0.0.0.0";
		Integer port = 61616;
		try
		{
			if (args.length <= 0) 
			{
				System.out.println("Usage: run.sh <render [port]|fractal [host:port]>");
			}
			else if (args[0].equals("render"))
			{
				if (args.length > 1) 
				{
					port = Integer.parseInt(args[1]);
				}
				JMS jms = new JMS(host, port, true);
				jms.start();			
				jms.registerService(new LogServiceImpl(), LogService.class);
				jms.registerService(new FractalServiceImpl(), FractalService.class);
				MainWindow mainWindow = new MainWindow(jms);
				mainWindow.setVisible(true);
				mainWindow.start();			
				System.in.read();
				jms.stop();
				System.exit(0);
			}
			else
			{
				if (args.length > 1)
				{
					String[] sa = args[1].split(":");
					host = sa[0];
					if (sa.length > 1) 
					{
						port = Integer.parseInt(sa[1]);
					}
				}
				JMS jms = new JMS(host, port, false);
				jms.start();				
				jms.registerService(new FractalServiceImpl(), FractalService.class);				
				System.in.read();
				jms.stop();
				System.exit(0);
			}
		}
		catch (Throwable t)
		{
			t.printStackTrace();
		}
	}
}