import os, time, datetime, subprocess, sublime, sublime_plugin

class RunMongoScriptCommand(sublime_plugin.TextCommand):
	def run_process(self, exe, script):    
		env = { 'PYTHONIOENCODING': 'utf-8' }
		p = subprocess.Popen(['sh', '-c', exe], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE, env=env)
		p.stdin.write(script)
		p.stdin.close()
		while(True):
			retcode = p.poll()			
			line = p.stdout.readline()
			if not (line.startswith('MongoDB') or line.startswith('connecting') or line.startswith('bye')):
				yield line.replace('\n', '')
			if(retcode is not None):				
				break			

	def run(self, edit):								
		if os.environ.get('MONGO_HOME') == None:
			print "$MONGO_HOME not defined!"
		else:
			text = self.view.substr(sublime.Region(0, self.view.size())).encode('utf8')
			first_line = text.split('\n', 1)[0]
			script = text.split('\n', 1)[1]			
			#Read connection parameters
			parameters = first_line.replace('//', '')			
			#Execute script
			exe = os.environ.get('MONGO_HOME') + '/bin/mongo ' + parameters
			
			out_panel = self.view.window().get_output_panel("mmongo_output")
			self.view.window().output_view = out_panel
			out_panel.set_encoding('utf-8')
			out_panel.set_read_only(False)			
			edit = out_panel.begin_edit()
			out_panel.erase(edit, sublime.Region(0, out_panel.size()))
			out_panel.insert(edit, out_panel.size(), 'Start: ' + time.strftime('%H:%M:%S') + '\n')
			for line in self.run_process(exe, script):
				out_panel.insert(edit, out_panel.size(), line.decode('utf-8') + '\n')

			out_panel.insert(edit, out_panel.size(), 'End: ' + time.strftime('%H:%M:%S') + '\n')
			out_panel.end_edit(edit)
			out_panel.set_read_only(True)
			self.view.window().run_command("show_panel", {"panel": "output.mmongo_output"})
