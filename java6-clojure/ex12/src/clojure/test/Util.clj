(ns test.Util
	(:gen-class    	
    	:methods [#^{:static true} [sqr [double] double]]
    )
)

(defn sqr [x] (* x x))
(defn -sqr [x] (sqr x))