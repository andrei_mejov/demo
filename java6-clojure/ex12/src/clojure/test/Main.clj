(ns test.Main
    (:gen-class)
    (:use test.Util)    
)

(import 
	[test.java BackgroundThread]
)

(defn -main []
    (println "QQQ")
    (println (sqr 3))
    (BackgroundThread/start)
)