package test.java;

import java.util.*;

public class BackgroundThread
{
	public static void start()
	{
		Thread t = new Thread(new Runnable() {
			public void run()
			{
				while (true)
				{
					System.out.println("==>" + new Date());
					try
					{
						Thread.sleep(1000L);
					}
					catch (Throwable x) {}
				}
			}
		});
		t.start();
	}
}