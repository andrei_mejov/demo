package test.unit.java;

import org.junit.*;
import static org.junit.Assert.*;

import test.*;
import test.java.*;

public class UtilTest
{
	@Test
	public void testSqr()
	{
		assertTrue(Util.sqr(3.0) == 9.0);
	}
}